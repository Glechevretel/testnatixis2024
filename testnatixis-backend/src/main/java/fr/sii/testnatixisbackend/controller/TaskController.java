package fr.sii.testnatixisbackend.controller;

import fr.sii.testnatixisbackend.dto.TaskDto;
import fr.sii.testnatixisbackend.mapper.TaskMapper;
import fr.sii.testnatixisbackend.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    private final TaskMapper taskMapper;

    @GetMapping
    public Page<TaskDto> getAllTasksPaginated(@PageableDefault Pageable pageable) {
        return taskService.findAllPaginated(pageable)
                .map(taskMapper::toDto);
    }

    @GetMapping("/{taskId}")
    public TaskDto getTaskById(final @PathVariable Long taskId) {
        return taskMapper.toDto(taskService.getTaskById(taskId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto createTask(final @RequestBody TaskDto taskDto) {
        return taskMapper.toDto(taskService.createTask(taskMapper.toEntity(taskDto)));
    }

    @PutMapping("/{taskId}")
    public TaskDto updateTask(final @PathVariable Long taskId, final @RequestBody TaskDto taskDto) {
        return taskMapper.toDto(taskService.updateTask(taskId, taskMapper.toEntity(taskDto)));
    }

    @DeleteMapping("/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(final @PathVariable Long taskId) {
        taskService.deleteTask(taskId);
    }
}

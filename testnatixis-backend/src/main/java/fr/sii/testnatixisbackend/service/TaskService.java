package fr.sii.testnatixisbackend.service;

import fr.sii.testnatixisbackend.entity.Task;
import fr.sii.testnatixisbackend.exception.SiiNotFoundException;
import fr.sii.testnatixisbackend.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    private static final String NOT_FOUND_CODE = "i18n.api.error.task.notFound";
    private static final String NOT_FOUND_MSG = "La tâche avec l'id: %s n'a pas été trouvée.";

    /**
     * Récupère toutes les tâches paginées.
     *
     * @param page Pageable
     * @return Liste des tâches paginées.
     */
    public Page<Task> findAllPaginated(Pageable page) {
        return taskRepository.findAll(page);
    }

    /**
     * Récupère une tâche par son ID.
     *
     * @param id Identifiant de la tâche.
     * @return Tâche trouvée ou {@link SiiNotFoundException}.
     */
    public Task getTaskById(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new SiiNotFoundException(NOT_FOUND_MSG));
    }

    /**
     * Crée une nouvelle tâche.
     *
     * @param task Tâche à créer.
     * @return Tâche créée.
     */
    public Task createTask(Task task) {
        //1- Vérifions si le libellé de la tâche n'existe pas déjà.
        if (isLabelUsed(null, task.getLabel())) {
            throw new IllegalArgumentException("Le libellé de la tâche existe déjà.");
        }

        //2- A sa création, la tâche, n'est pas complétée.
        task.setComplete(false);

        return taskRepository.save(task);
    }

    /**
     * Met à jour une tâche.
     *
     * @param task Tâche à mettre à jour.
     * @return Tâche mise à jour.
     */
    public Task updateTask(Long taskId, Task task) {
        //1- Vérfions l'existance de la tâche.
        getTaskById(taskId);

        //2- Vérifions si le libellé de la tâche n'existe pas déjà.
        if (isLabelUsed(taskId, task.getLabel())) {
            throw new IllegalArgumentException("Le libellé de la tâche existe déjà.");
        }

        task.setId(taskId);
        return taskRepository.save(task);
    }

    /**
     * Supprime une tâche par son ID.
     *
     * @param id Identifiant de la tâche.
     */
    public void deleteTask(Long id) {
        // Vérifions l'existance de la tâche.
        getTaskById(id);

        taskRepository.deleteById(id);
    }

    /**
     * Détermine si le label d'une tâche est déjà utilisé.
     *
     * @param taskId l'ID de la tâche.
     * @param label  le label de la tâche.
     * @return vrai si le label est utilisé et qu'il ne s'agit pas de la tâche possèdant le label. Faux sinon.
     */
    private boolean isLabelUsed(Long taskId, String label) {
        Optional<Task> task = taskRepository.findByLabel(label);
        if (task.isPresent() && task.get().getId().equals(taskId)) {
            return false;
        }
        return taskRepository.existsByLabel(label);
    }
}

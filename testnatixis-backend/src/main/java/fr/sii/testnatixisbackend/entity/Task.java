package fr.sii.testnatixisbackend.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "label", nullable = false)
    private String label;

    @Column(name = "complete", nullable = false)
    private Boolean complete;

    public Task(String label, Boolean complete) {
        this.label = label;
        this.complete = complete;
    }

    @Override
    public String toString()
    {
        return "Tache [id="
                + id + ", label="
                + label + ", complete="
                + complete + "]";
    }

}

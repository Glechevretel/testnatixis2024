package fr.sii.testnatixisbackend.dto;

import lombok.Builder;

@Builder(toBuilder = true)
public record TaskDto(
        Long id,
        String label,
        Boolean complete
){
}

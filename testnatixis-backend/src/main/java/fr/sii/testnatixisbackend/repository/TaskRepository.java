package fr.sii.testnatixisbackend.repository;

import fr.sii.testnatixisbackend.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {
    boolean existsByLabel(String label);

    Optional<Task> findByLabel(String label);
}

package fr.sii.testnatixisbackend.mapper;

import fr.sii.testnatixisbackend.dto.TaskDto;
import fr.sii.testnatixisbackend.entity.Task;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper {

    TaskDto toDto(Task task);

    Task toEntity(TaskDto taskDto);

    List<TaskDto> toDtos(List<Task> taskList);

    List<Task> toEntities(List<TaskDto> taskDtoList);
}

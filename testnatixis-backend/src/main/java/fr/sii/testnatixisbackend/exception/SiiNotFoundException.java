package fr.sii.testnatixisbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SiiNotFoundException extends RuntimeException{

    public SiiNotFoundException() {
        super();
    }
    public SiiNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    public SiiNotFoundException(String message) {
        super(message);
    }
    public SiiNotFoundException(Throwable cause) {
        super(cause);
    }
}

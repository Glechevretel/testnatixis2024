package fr.sii.testnatixisbackend.configuration;

import fr.sii.testnatixisbackend.entity.Task;
import fr.sii.testnatixisbackend.service.TaskService;
import net.datafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(TaskService taskService) {
        return args -> {
            Faker faker = new Faker();

            for (int i = 0; i < 27; i++) {
                String name = faker.lorem().sentence(3);
                boolean complete = faker.bool().bool();
                taskService.createTask(new Task(name, complete));
            }
        };
    }
}

package fr.sii.testnatixisbackend;

import fr.sii.testnatixisbackend.dto.TaskDto;
import fr.sii.testnatixisbackend.entity.Task;
import fr.sii.testnatixisbackend.mapper.TaskMapper;
import fr.sii.testnatixisbackend.repository.TaskRepository;
import fr.sii.testnatixisbackend.service.TaskService;
import lombok.RequiredArgsConstructor;
import net.datafaker.Faker;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
@RequiredArgsConstructor
public class TaskTestHelper {
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final TaskService taskService;

    /**
     * Génération de données aléatoires.
     */
    static Faker faker = new Faker(Locale.FRANCE);

    /**
     * Crée une nouvelle tâche.
     *
     * @return la {@link TaskDto} créée.
     */
    public TaskDto createTask() {
        TaskDto taskDto = generateTaskDto();
        Task task = taskMapper.toEntity(taskDto);
        return taskMapper.toDto(taskService.createTask(task));
    }

    /**
     * Crée un nombre aléatoire de {@link TaskDto}.
     *
     * @param count Nombre de tâches à créer.
     * @return la liste des {@link TaskDto} créées.
     */
    public List<TaskDto> createTasks(int count) {
        List<TaskDto> taskDtos = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            taskDtos.add(createTask());
        }
        return taskDtos;
    }

    /**
     * Supprime une tâche par son ID.
     *
     * @param id Identifiant de la tâche à supprimer.
     */
    public void deleteTask(Long id) {
        taskService.deleteTask(id);
    }

    /**
     * Supprime toutes les tâches.
     */
    public void deleteAllTasks() {
        taskRepository.deleteAll();
    }

    /**
     * Génère un dto pour une tâche aléatoire.
     *
     * @return
     */
    public TaskDto generateTaskDto() {
        return TaskDto.builder()
                .label(faker.lorem().sentence(10))
                .complete(false)
                .build();
    }
}

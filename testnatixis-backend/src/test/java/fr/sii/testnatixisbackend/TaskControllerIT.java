package fr.sii.testnatixisbackend;

import fr.sii.testnatixisbackend.dto.TaskDto;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.with;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class TaskControllerIT {

    @Autowired
    private TaskTestHelper taskTestHelper;

    @LocalServerPort
    private int port;


    @Test
    void whenGettingAllTasks_thenTasksArePaginated() {

        // On vide la bdd (pour éviter l'injection au démarrage de l'application.
        taskTestHelper.deleteAllTasks();

        //Si plusieurs tâches en bdd.
        List<TaskDto> tasks = taskTestHelper.createTasks(7);

        //Alors, lors de la requêtes pour la liste des tâches.
        Response response = given().port(port)
                .queryParam("page", 0)
                .queryParam("size", 10)
                .queryParam("buyersIds", "")
                .queryParam("search", "")
                .when()
                .get("/tasks")
                .then()
                .statusCode(HttpStatus.OK.value())
                .log().ifError()
                .extract()
                .response();

        // La liste contient le bon nombre d'éléments
        assertThat(response.jsonPath().getInt("totalElements")).isEqualTo(7);

        //On supprime les tâches créées.
        tasks.forEach(task -> taskTestHelper.deleteTask(task.id()));
    }

    @Test
    void whenGettingTaskById_thenTaskIsReturned() {

        //On crée une tâche.
        TaskDto taskDto = taskTestHelper.createTask();

        //Si l'on effectue une requête pour la récupérée.
        TaskDto foundTaskDto = given().port(port)
                .when()
                .get("/tasks/" + taskDto.id())
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(TaskDto.class);

        // La tâche correspond bien à celle créee.
        assertThat(foundTaskDto).usingRecursiveComparison()
                .isEqualTo(taskDto);

        // On détruit la valeur créee.
        taskTestHelper.deleteTask(foundTaskDto.id());
    }

    @Test
    void whenCreatingTask_thenTaskIsCreated() {

        //On génère le dto d'une tâche.
        TaskDto taskDto = taskTestHelper.generateTaskDto();

        //Alors, la tâche est bien créée.
        TaskDto createdTaskDto = given().port(port)
                .contentType("application/json")
                .body(taskDto)
                .when()
                .post("/tasks")
                .then()
                .log().ifError()
                .statusCode(HttpStatus.CREATED.value())
                .extract()
                .as(TaskDto.class);

        //La tâche est bien créée.
        // Alors nous devons avoir l'acheteur créé.
        assertThat(createdTaskDto).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(taskDto);

        // On détruit la valeur créee.
        taskTestHelper.deleteTask(createdTaskDto.id());
    }

    @Test
    void whenUpdatingTask_thenTaskIsUpdated() {

        //On crée une tâche.
        TaskDto taskDto = taskTestHelper.createTask();

        //On souhaite modifier la tâche.
        TaskDto taskDtoToUpdate = taskTestHelper.generateTaskDto();

        //Alors, la tâche est bien modifiée.
        TaskDto updatedTaskDto = given().port(port)
                .contentType("application/json")
                .body(taskDtoToUpdate)
                .when()
                .put("/tasks/" + taskDto.id())
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(TaskDto.class);

        //La tâche est bien modifiée.
        // Alors nous devons avoir l'acheteur modifié.
        assertThat(updatedTaskDto).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(taskDtoToUpdate);

        // On détruit la valeur créee.
        taskTestHelper.deleteTask(updatedTaskDto.id());
    }

    @Test
    void whenDeletingTask_thenTaskIsDeleted() {
        //On crée une tâche.
        TaskDto taskDto = taskTestHelper.createTask();

        //Si l'on effectue une requête pour la supprimer.
        with().port(port)
                .when()
                .delete("/tasks/" + taskDto.id())
                .then()
                .log().ifError()
                .statusCode(HttpStatus.NO_CONTENT.value());

        //On vérifie que la tâche n'existe plus.
        given().port(port)
                .when()
                .get("/tasks/" + taskDto.id())
                .then()
                .log().ifError()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }
}

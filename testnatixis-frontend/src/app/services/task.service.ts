import { map, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  public baseURL = "http://localhost:8080/tasks";

  constructor(private http : HttpClient) { }

  getAllTasks(
    page: number,
    size: number,
    sortField?: string,
    sortOrder?: string,
  ): Observable<any> {
    const params = {
      page,
      size,
      ...(sortField && sortOrder && { sort: `${sortField},${sortOrder}` })
  };
    return this.http.get<Task[]>(`${this.baseURL}`,{params});
  }

  getAllTasks2(): Observable<any> {
    return this.http.get<Task[]>(`${this.baseURL}`);
  };

  createTask(task: Task): Observable<any>{
    return this.http.post(`${this.baseURL}`, task);
  }

  getTaskById(id: number): Observable<Task>{
    return this.http.get<Task>(`${this.baseURL}/${id}`);
  }

  updateTask(id: number, task: Task): Observable<Object>{
    return this.http.put(`${this.baseURL}/${id}`, task);
  }

  deleteTask(id: number): Observable<Object>{
    return this.http.delete(`${this.baseURL}/${id}`,{ responseType: 'text' });
  }

}

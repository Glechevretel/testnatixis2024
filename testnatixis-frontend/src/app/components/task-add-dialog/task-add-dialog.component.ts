import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {Task} from 'src/app/models/task';
import {TaskService} from 'src/app/services/task.service';

@Component({
  selector: 'app-task-add-dialog',
  templateUrl: './task-add-dialog.component.html',
})
export class TaskAddDialogComponent {
  task: Task = new Task();

  constructor(
    public dialogRef: MatDialogRef<TaskAddDialogComponent>,
    private taskService: TaskService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.taskService.createTask(this.task).subscribe(() => {
      this.dialogRef.close();
    });
  }
}

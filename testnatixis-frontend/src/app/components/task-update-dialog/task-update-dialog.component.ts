import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Task} from 'src/app/models/task';
import {TaskService} from 'src/app/services/task.service';

@Component({
  selector: 'app-task-update-dialog',
  templateUrl: './task-update-dialog.component.html',
})
export class TaskUpdateDialogComponent {
  task: Task;

  constructor(
    public dialogRef: MatDialogRef<TaskUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Task,
    private taskService: TaskService
  ) {
    this.task = {...data};
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.taskService.updateTask(this.task.id, this.task).subscribe(() => {
      this.dialogRef.close();
    });
  }
}

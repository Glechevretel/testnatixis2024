import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';

import { TaskUpdateDialogComponent } from './task-update-dialog.component';
import { TaskService } from 'src/app/services/task.service';

describe('TaskUpdateDialogComponent', () => {
  let component: TaskUpdateDialogComponent;
  let fixture: ComponentFixture<TaskUpdateDialogComponent>;
  let taskService: TaskService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ TaskUpdateDialogComponent ],
      providers: [
        { provide: MatDialogRef, useValue: { close: () => {} } },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskUpdateDialogComponent);
    component = fixture.componentInstance;
    taskService = TestBed.inject(TaskService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call updateTask on submit', () => {
    const task = { id: 1, label: 'Test', complete: false };
    component.task = task;
    spyOn(taskService, 'updateTask').and.returnValue(of(task));
    component.onSubmit();
    expect(taskService.updateTask).toHaveBeenCalledWith(task.id, task);
  });
});

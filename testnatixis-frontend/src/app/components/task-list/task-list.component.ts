import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Task} from 'src/app/models/task';
import {TaskService} from 'src/app/services/task.service';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {MatDialog} from '@angular/material/dialog';
import {TaskAddDialogComponent} from '../task-add-dialog/task-add-dialog.component';
import {TaskUpdateDialogComponent} from '../task-update-dialog/task-update-dialog.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  displayedColumns: string[] = ['position', 'label', 'complete', 'action'];
  dataSource = new MatTableDataSource<Task>();
  rowSize = 10;
  defaultSort = 'id';
  totalRecords = 0;
  tasks: Task[] = [];


  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort!: MatSort;


  constructor(private taskService: TaskService, private router: Router, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getTasks();
  }

  updateComplete(task: Task): void {
    this.taskService.updateTask(task.id, task).subscribe();
  }

  deleteTask(id: number) {
    this.taskService.deleteTask(id).subscribe(data => {
      console.log(data);
      this.getTasks();
    })
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(TaskAddDialogComponent);

    dialogRef.afterClosed().subscribe(() => {
      this.getTasks();
    });
  }

  public getTasks(event?: PageEvent) {
    const currentPage = event ? event.pageIndex : 0;
    const rowsPerPage = event ? event.pageSize : this.rowSize;
    const sortField = this.defaultSort;
    const sortOrder = 'asc';

    this.taskService.getAllTasks(
      currentPage,
      rowsPerPage,
      sortField,
      sortOrder)
      .subscribe(data => {
        this.dataSource.data =  data.content;
        this.totalRecords = data.totalElements;
        this.tasks = data.content;
      })

  }

  openUpdateDialog(task: Task): void {
    const dialogRef = this.dialog.open(TaskUpdateDialogComponent, {
      data: task
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getTasks();
    });
  }

  addFilter(change: MatCheckboxChange) {
    const filterValue = change.checked;

    if (filterValue) {
      this.dataSource.filterPredicate = (data: Task, filter: string) => {
        return !data.complete;
      };
    } else {
      this.dataSource.filterPredicate = (data: Task, filter: string) => {
        return true;
      };
    }

    this.dataSource.filter = String(Math.random());
  }

  pageChanged(event: PageEvent) {
    this.getTasks(event);
  }
}

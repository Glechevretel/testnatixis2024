export class Task {
    id!: number;
    label!: string;
    complete!: boolean;
}
